\section{Method}

% bullet points
%
%\begin{itemize}
%\item general CNN intro (2 sentences)
%\item "we normalize and stack the input video"
%\item Figure 1: Input $\rightarrow$ Network $\rightarrow$ Output
%\item exact description of network, Learning rate, augmentation
%\item formulas for the general optimization [J(W,b)]
%\item explain how to generate two outputs, and for what purpose
%\item show and differentiate template versions of the different network-types
%\end{itemize}

We use a CNN to model the relation between epilepsy patient recordings as input and the probability of a seizure event as output.
Conversely to previous methods, we rely on a single-frame approach.
Patients show unnatural postures during clonic, tonic or general convulsive seizures, which can be detected by our method even if no motion is present.
In Sec.\ 4 we show that the detection accuracy improves upon state-of-the-art by a large margin, without leveraging temporal consistency.
%In order to learn possible motion features obtained from the comparison of temporally correlated frames, we compute spatio-temporal input volumes from these data streams.
%, the spatial extension being as large as the input frame resolution, while the temporal extension ranging from 1 to n frames. 
%The objective is to classify each frame as belonging to either \textit{seizure} or \textit{non-seizure} class.
To create our model, we preprocess the input data (Sec.~\ref{subsec:Pre}), define the network architecture (Sec.~\ref{subsec:NetAr}) and finally train the CNN in a supervised fashion (Sec.~\ref{subsec:Train}).

\subsection{Preprocessing}\label{subsec:Pre}
%Convolutional operations result in a high output value, when the input image structure at the common support region resembles the filter structure.
Convolution is a linear operation, which results in the output being inherently sensitive to peak input values. %find the citation for this
In other terms, feature extraction by convolution will always favor bright image regions.
%When processing two different modalities with the same CNN, it is therefore necessary to normalize their domains.
We give an equal \textit{a priori} weight to both input modalities by normalizing their domains.
The depth sensor provides a video stream $I_{\mathrm{D}}$ whose pixel values represent distances between $500$\,mm and $4500$\,mm at 1\,bit/mm resolution.
In the IR stream $I_{\mathrm{IR}}$ however, values are not limited and extend to the full 16-bit range [0-65525].
$I_{\mathrm{IR}}$ is preprocessed with a natural logarithm in order to decrease the high dynamic range.
$I_{\mathrm{D}}$ and $\hat{I}_{\mathrm{IR}} = \log \left(1+I_{\mathrm{IR}}\right) $ are then rescaled to the range [0-255].
In order to discard spurious noise in the depth stream $I_{\mathrm{D}}$ (\eg at reflective surfaces or occluding edges) we apply a [$3\times3$] median filter kernel and obtain $\hat{I}_{\mathrm{D}}$.

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{NetworkFusionOptions.pdf}
\caption{Combining depth and IR input.
(A): Depth and IR are processed with individual CNNs only trained on the respective modality.
Both CNNs follow the same architecture, detailed in Fig.~\ref{fig:architecture}.
Final seizure probability $q$ is obtained by multiplying $q_\mathrm{D}$ from the depth-CNN and $q_\mathrm{IR}$ from the IR-CNN.
(B): The CNN is trained on a 2-channel input built by combining a depth-frame and an IR-frame.
Network architecture is the same as in type \emph{A} but processes two different input modalities in its first filter layer.}
\label{fig:combination}
\end{figure}
%\begin{figure}[t]
%\centering
%\subfigure[\label{fig:combinationA}In network A, depth and IR are processed with individual CNNs, each trained on the respective modality.
%Both CNNs follow the same architecture, detailed in Fig.~\ref{fig:architecture}.
%Final seizure probability $q$ is obtained by multiplying $q_\mathrm{D}$ and $q_\mathrm{IR}$.]
%{\centering \includegraphics[width=0.47\textwidth]{images/CombSchemeA.pdf}
%}
%\qquad
%\subfigure[\label{fig:combinationB}In network B, the CNN is trained on a 2-channel input built by combining a depth-frame and an IR-frame.
%The CNN architecture is the same as for type A but processes two different input modalities through the first filter layer.]
%{\centering
%\includegraphics[width=0.47\textwidth]{images/CombSchemeB.pdf}
%}
%\caption{Combining depth and infrared with two different schemes (A and B).}
%\label{fig:combination}
%\end{figure}

%\begin{table}
%\caption{Sizes of filters and feature maps of the network. $N$ is the size of the temporal window, $n$ the number of binary output variables.}
%\begin{tabular}{c|c|c|c|c|c|c}
%layer & conv(1) & conv(2) & conv(3) & fc(4) & fc(5) & fc(6) \\ 
%\hline 
%input map & 100x100x[2$N$] & 46x46x16 & 20x20x64 & 8x8x128 & 1x1x1024 & 1x1x512 \\ 
%output map & 92x92x16 & 40x40x64 & 16x16x128 & 1x1x1024 & 1x1x512 & $n$x1x2 \\ 
%2D conv size & 9x9 & 7x7 & 5x5 & - & - & - \\ 
%\end{tabular} 
%\label{tab:networkParams}
%\end{table}

\subsection{Network Architecture}\label{subsec:NetAr}
The network architecture that is best suited to the task at hand is depicted in Fig.~\ref{fig:architecture}.
Input to the network are IR and depth frames $\{ \hat{I}_{\mathrm{IR}}, \hat{I}_{\mathrm{D}} \}$.
Depending on the combination scheme that is used, they are either processed individually (type A) or stacked to build up one single frame with two channels (type B), see Fig.~\ref{fig:combination}.
\begin{figure}
\centering
\includegraphics[width=\columnwidth]{images/NetworkInside.pdf}
\caption{Network architecture.
Layer conv(1) extracts local features through convolution with eight $[5\times5]$ filter kernels.
%Max pooling with $[12\times12]$ receptive fields downscales the input to a $8$\,px$\times8$\,px feature map.
Max pooling downsamples the input to a $8$\,px$\times8$\,px feature map.
Fully connected layers fc(2-3) condense the feature map into a binary output, yielding the seizure probability $q$.}
\label{fig:architecture}
\end{figure}
The input frames originally have a resolution of $512\,\mathrm{px}\times424\,\mathrm{px}$.
For training and testing, they are center-cropped to $424\,\mathrm{px}\times424\,\mathrm{px}$ and downscaled to $100\,\mathrm{px}\times100\,\mathrm{px}$.
\todo{This was criticized by one of the workshop reviewers. Either explain 'center-cropped and downscaled' better verbally or make a small figure about the preprocessing-steps.}
%along the temporal dimension from $t_{i-N+1}$ to $t_{i}$, where $i$ represents the index of the current frame and $N$ the number of frames in the spatio-temporal window.
%Commonly, CNNs are used to process RGB images in which each color defines an input channel~\cite{krizhevsky2012}.
%In our architecture however, the channels consist of $N$ IR and depth frame pairs stacked in the time domain, so that instead of three we process $2\cdot N$ input channels.
We compute an average frame for IR and depth respectively by summing over all available samples in the training set and dividing by the number of samples.

After subtracting the average frame $\{ \mu_{\mathrm{IR}}^{\mathrm{train}}, \mu_{\mathrm{D}}^{\mathrm{train}} \}$ for each modality, the input is transformed into a feature map through the first computational block consisting of 1) a convolutional layer with stride 1, 2) a rectified linear unit (ReLU), 3) a max pooling layer with stride 12 and 4) a local response normalization layer as the one used by~\cite{krizhevsky2012}.
%with window size 5, $\alpha=5\cdot10^{-4}$, $\beta=0.75$ and $\kappa=1$ 
In the second computational block, two subsequent fully connected layers with ReLU activation units reduce the size of the feature map so to build up a two-element vector.
This vector is normalized into $[0,1]$ by a softmax operation, see equation~(\ref{eqn:softmaxloss}).
Finally, the CNN output is $\left[ q,~1-q \right]$, where $q$ represents the probability for the seizure event to be \textit{true} and reciprocally $1-q$ describes the probability for the event to be \textit{false}.
%By reconfiguring the fully connected layer fc(6) it is possible to provide $n$ binary outputs which allows for the detection of multiple simultaneous events in the scene.

\subsection{Training}\label{subsec:Train}
In order to learn the connection weights and biases for the convolutional and the fully connected layers, we append a \textit{logarithmical softmax loss} layer to the network output:
\begin{equation}
L(x,y) = -\frac{1}{n} \sum_i \log \left(\frac{e^{x_{i,y(i)}}}{\sum_c e^{x_{i,c}}} \right)
\label{eqn:softmaxloss}
\end{equation}
where $x$ is the last feature map of the network, $y$ contains the ground truth class for each output variable and $n$ is the number of output variables.
Through the loss layer, decisions for the wrong binary class are penalized and the derivative with respect to the output $x$ is calculated.
This derivative is back-propagated through the network, using stochastic gradient descent (SGD) for loss minimization.
For all iterations, a batch-size of 250 samples and a learning-rate of $10^{-3.25}$ are chosen.
The rest of the parameters is set according to~\cite{krizhevsky2012}.
In particular, the momentum is set to $0.9$ and weight decay for L$_2$ regularisation is $5\cdot10^{-4}$.
During training, we apply a dropout of 0.5 before fc(2) and fc(3).
%\begin{align*}
%\label{eqn:LossAndGradientDescent}
%\frac{\partial L(x,y)}{\partial x_i} &= \frac{1}{n} \cdot \left\{
%  \begin{array}{l l}
%    \dfrac{e^{x_{i,\tilde{c}}}}{\sum_c e^{x_{i,c}}} - 1 & \quad \text{if } \tilde{c}=y(i)\\
%    \dfrac{e^{x_{i,\tilde{c}}}}{\sum_c e^{x_{i,c}}} & \quad \text{otherwise}
%  \end{array} \right.
%\\
%\\
%\frac{\partial L}{\partial W_{n}} &= \frac{\partial L}{\partial X_m} \cdot \frac{\partial X_m}{\partial X_{m-1}} \cdots \frac{\partial X_{n+2}}{\partial X_{n+1}} \cdot \frac{\partial X_{n+1}}{\partial W_{n}}
%\\
%\\
%W_{1..n}(\tau+1) &= W_{1..n}(\tau) + \mu\cdot\frac{\partial L}{\partial W_{1..n}(\tau)}
%\end{align*}

%\subsection{Data Augmentation}
We augment the training data in order to increase the generalization properties of our model.
More specifically, we horizontally flip half of the images in each batch, and randomly shuffle the training set before each SGD epoch.
%During one epoch, SGD iterates through all training batches.
%An epoch is typically defined as the period that SGD requires for iterating through all training batches.
Using real patient recordings for training, we cannot guarantee a 50/50 ratio between positive and negative training samples: indeed, the problem at hand is intrinsically unbalanced, since negative frame samples outnumber the positive ones on typical recordings. 
In order to achieve a balanced training set, one could select the class with fewer available samples and select an equal amount of samples from the over-represented class.
This however results in discarding a lot of valuable samples, hence distorting the input distribution which may lead to a loss of generality.
In contrast, we determine the class $c_{-}\in \mathbf{C}=\{ c_{\mathrm{true}}, c_{\mathrm{false}} \}$ which has fewer available training samples and randomly pick the same amount of samples from class $c_+ = \mathbf{C}\setminus c_{-}$ before each epoch.
After one epoch is trained using SDG, a different set of samples is randomly picked from $c_+$ and combined with the samples from $c_{-}$.
This way, the network is trained along gradients from positive and negative samples at an equal rate while at the same time using all of the available training data.

